# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.6.5](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.6.4...rma-warranty@1.6.5) (2021-04-12)

**Note:** Version bump only for package rma-warranty





## [1.6.4](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.6.3...rma-warranty@1.6.4) (2021-03-24)


### Bug Fixes

* **rma-warranty:** Filters fixed on Warranty Page ([4457d93](https://gitlab.com/castlecraft/excel-rma/commit/4457d93cda56a17e8962cd7d0cda4df4cf89fe94))





## [1.6.3](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.6.2...rma-warranty@1.6.3) (2021-03-13)

**Note:** Version bump only for package rma-warranty





## [1.6.2](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.6.1...rma-warranty@1.6.2) (2021-03-10)


### Bug Fixes

* **rma-warranty:** Fixed filters on Create Service Invoice page ([9de2a79](https://gitlab.com/castlecraft/excel-rma/commit/9de2a79a6db3bc8a492d057c8185d628a22a5de1))
* **rma-warranty:** Fixed filters on Create Service Invoice page 1 ([59819a8](https://gitlab.com/castlecraft/excel-rma/commit/59819a82c3e029c3c321492737172328a4525275))





## [1.6.1](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.6.0...rma-warranty@1.6.1) (2021-03-09)

**Note:** Version bump only for package rma-warranty





# [1.6.0](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.5.1...rma-warranty@1.6.0) (2021-03-08)


### Bug Fixes

* **rma-warranty:** Fixed all filters on Warranty page ([361aa73](https://gitlab.com/castlecraft/excel-rma/commit/361aa7377321e30ac2030b11f9acf8a5590e262c))
* **warranty-UI:** Fix add warranty claim creation page ([8bcd315](https://gitlab.com/castlecraft/excel-rma/commit/8bcd31550cc2414cbb9170516789a87ad48eb3d0))


### Features

* **Excel-Warranty:** Fix bugs on add warranty claim ([18af6e4](https://gitlab.com/castlecraft/excel-rma/commit/18af6e40fbfa99a3ceccc81a5f8bce7cba0df277))





## [1.5.1](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.5.0...rma-warranty@1.5.1) (2021-03-06)

**Note:** Version bump only for package rma-warranty





# [1.5.0](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.4.1...rma-warranty@1.5.0) (2021-02-27)


### Features

* **rma-warranty:** Add favIcon to Warranty App ([7131bd2](https://gitlab.com/castlecraft/excel-rma/commit/7131bd2055e02185e60b38cad51ef8437334ec41))
* **rma-warranty:** Add favIcon to Warranty App 1 ([d40dd26](https://gitlab.com/castlecraft/excel-rma/commit/d40dd26afdc584ae8834b539e632bc4447985d2c))





## [1.4.1](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.4.0...rma-warranty@1.4.1) (2021-02-21)

**Note:** Version bump only for package rma-warranty





# [1.4.0](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.3.0...rma-warranty@1.4.0) (2021-02-18)


### Features

* **Excel-rma:** Add brand settings and favicon in global defaults ([7aa4c8f](https://gitlab.com/castlecraft/excel-rma/commit/7aa4c8f7f4a05ea3db72bfba7c4c46fe688d9d4a))





# [1.3.0](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.2.0...rma-warranty@1.3.0) (2021-02-09)


### Features

* **rma-warranty:** Clear filter button added on Serial Search page ([fa24983](https://gitlab.com/castlecraft/excel-rma/commit/fa2498327fa035158c3a5e74039dc9eff17f1731))
* **rma-warranty:** Token Generation added on Warranty page and updated on Frontend page ([6fe3a8a](https://gitlab.com/castlecraft/excel-rma/commit/6fe3a8a1914e3dde070052c47b69dfdb924bf24d))





# [1.2.0](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.1.1...rma-warranty@1.2.0) (2021-02-03)


### Features

* **rma-warranty:** logout from erp on logout from custom app ([d7303e3](https://gitlab.com/castlecraft/excel-rma/commit/d7303e3fa01d653cdd3e9f6d635b376e6dfa4693))





## [1.1.1](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.1.0...rma-warranty@1.1.1) (2021-01-27)


### Bug Fixes

* **excel-rma:** Add fixes for item-price, stock, webhooks ([ce7c597](https://gitlab.com/castlecraft/excel-rma/commit/ce7c597cfd14691cabd0cba66a0cfec080ada4df))





# [1.1.0](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.0.1...rma-warranty@1.1.0) (2021-01-23)


### Features

* **rma-frontend:** Single Serial search and combined Item and Warehouse search added ([62f5a95](https://gitlab.com/castlecraft/excel-rma/commit/62f5a957089f4aae0eb5a5e38920aec1226370c9))





## [1.0.1](https://gitlab.com/castlecraft/excel-rma/compare/rma-warranty@1.0.0...rma-warranty@1.0.1) (2021-01-23)

**Note:** Version bump only for package rma-warranty
